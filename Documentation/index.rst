.. NewEgg Product Review API documentation master file, created by
   sphinx-quickstart on Mon Nov 11 14:10:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NewEgg Product Review API's documentation!
=====================================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Getting Started
===============

The NewEgg Product Review API allows product manufacturers to query all reviews of their products sold on the website `www.newegg.com <http://www.newegg.com>`_. With the help of the site's public API which allows users to quickly pull JSON formatted review data, we have generated a MongoDB database which is hosted on Heroku, and provided an intuitive User Interface to make querying much more easily accessible.  

Running the API Locally
=======================

To run the service locally you must first have MongoDB installed, and then the scripts from our repository located at `https://bitbucket.org/sreisman/newegg-api-webservice/overview <https://bitbucket.org/sreisman/newegg-api-webservice/overview>`_, can be ran to populate your database.  An example pipeline would first pull data from the NewEgg website using the get_all_products_info.py file, and inserted into a local mongo instance using the mongo_import.py script.  With our directory structure this would look like the following::

	python get_all_products_info.py
	cd backend
	cat AllProducts.csv | python ../mongo_import.py products

Here, in the last line, "products" is the name you specify for the database.  If the database specified already exists, the code will not run so that you don't overwrite data.  You must specifiy a new database name to import new data.  


Webservice URL:  

	http://newegg-api-webservice.herokuapp.com/

API Documentation
==================

POST http://newegg-api-webservice.herokuapp.com/
################################################  

* Returns a new login-id(UUID) to the user to use in text/plain  
* _Request body_: empty  
* _Response body_: UUID in text/plain (e.g. "8b163c22-20a7-469e-8ac7-1c7a6acefd32")  

GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/
########################################################################  

* Gets a list of this id's products in JSON format.  
*
* _Response body_: List of products in JSON format. See appendix 1

POST http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/
############################################################################    

* Add new products in JSON format to the list of products for this id. If  
* a product or all products already exist in this id's list, the  
* product(s) will not be added.  
* _Request body_: List of products in JSON format. See appendix 1  
* _Response body_: List of products in JSON format. See appendix 1  

GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/reviews?star-or-less=__{stars-or-less}__&reviews-since=__{reviews-since}__
##################################################################################################################################  

* Get reviews for all products with the specified parameters
* _Response body_: List of reviews in JSON format. See appendix 2  

GET  http://newegg-api-webservice.herokuapp.com/__{login-id}__/products/__{product-id}__/reviews?star-or-less=__{stars-or-less}__&reviews-since=__{reviews-since}__
#################################################################################################################################

* Get reviews for this product with the specified parameters
* _Response body_: List of reviews in JSON format. See appendix 3  

Parameters
##########

* __{login-id}__ - Required. Your login-id given to you when you POST to the base URI.  
* __{stars-or-less}__ - Not required. A number between 1 and 5 to return all reviews with {stars-or-less} stars or less.  
* __{reviews-since}__ - Not required. A timestamp in javascript form (number of seconds since Jan. 1, 1970 to return all reviews since {reviews-since}.  

Appendices
##########

	Appendix 1:: 
		{  
		  "products":[  
		    { "product":"1234" }  
		    { "product":"5678" }  
		  ]  
		}  

	Appendix 2:  
	{  
	  "products":[  
	    { "product": {
              "id":"1234",  
              "review-count":"25",
              "reviews":[  
                { "review":{  
		  "Rating":"2",  
		  "PublishDate":"8/28/2013 5:20:21 AM",
		  "LoginNickName":"John" }
                }
	      ]}
	    }
            { "product":{
              "id":"5678",
              "review-count":"57",
              "reviews":[
                ...
              ]}
            }
	  ]
	}
	    
	Appendix 3:  
	{
	  "product":{
	    "id":"1234",
            "review-count":"25",
	    "reviews":[
	      { "review":{
                "Rating":"2",
                "PublishDate":"8/28/2013 5:20:21 AM",
                "LoginNickName":"John" }
	      }
	    ]
	  }
	}

Connecting to Heroku's MongoDB
==============================

https://github.com/mongolab/mongodb-driver-examples/blob/master/python/pymongo_simple_example.py